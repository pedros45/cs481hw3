import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Recipe Home Page",
      home: MainPageWidget(),
    );
  }
}

int _selectedIndex = 0;
bool _isFavorite = false;

class MainPageWidget extends StatefulWidget {
  MainPageWidget({Key key}): super(key: key);

  @override
  _MainPageWidgetState createState() =>
      _MainPageWidgetState();

}

class _MainPageWidgetState extends State<MainPageWidget> {

  void _onItemTapped(int index){
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Welcome to Flutter',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Recipes For Me'),
        ),

        body: ListView(
          padding: const EdgeInsets.fromLTRB(0, 0, 0, 5),
          scrollDirection: Axis.vertical,
          children: <Widget> [
            Container(
              height: 350,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    'images/oatmeal.jpg',
                    width: 350,
                    height: 220,
                    fit: BoxFit.cover,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("Title of Recipe"),
                      IconButton(
                        icon: (_isFavorite ? Icon(Icons.favorite) : Icon(Icons.favorite_border)),
                        onPressed: () {
                          setState(() {
                            _isFavorite = !_isFavorite;
                          });
                        },
                      ),
                    ],
                  ),
                  Text("This is a simple description of a recipe."),
                ],
              ),
            ),
            Container(
              height: 350,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    'images/image2.jpg',
                    width: 350,
                    height: 220,
                    fit: BoxFit.cover,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("Title of Recipe 2"),
                      IconButton(
                        icon: (_isFavorite ? Icon(Icons.favorite) : Icon(Icons.favorite_border)),
                        onPressed: () {
                          setState(() {
                            _isFavorite = !_isFavorite;
                          });
                        },
                      ),
                    ],
                  ),
                  Text("This is another simple description of a recipe."),
                ],
              ),
            ),
          ],
        ),

        bottomNavigationBar: BottomNavigationBar(

          items: const <BottomNavigationBarItem> [
            BottomNavigationBarItem(
                icon: Icon(Icons.wb_sunny),
                title: Text("Breakfast"),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.fastfood),
              title: Text("Lunch"),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.local_dining),
              title: Text("Dinner"),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.free_breakfast),
              title: Text("Drinks"),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.favorite),
              title: Text("Favorites"),
            ),
          ],
          backgroundColor: Colors.blue,
          currentIndex: _selectedIndex,
          selectedItemColor: Colors.amber[800],
          unselectedItemColor: Colors.blue,
          onTap: _onItemTapped,
          showUnselectedLabels: true,
        ),

      ),
    );
  }
}
